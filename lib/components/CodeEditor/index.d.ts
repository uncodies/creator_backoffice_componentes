import React from 'react';
import './CodeEditor.css';
type ThemeType = 'light' | 'vs-dark';
type CodeEditorParams = {
    name: string;
    language: string;
    theme: ThemeType;
    readOnly?: boolean | string;
    value: string;
    getInformation: (key: string, value: string, errors: Array<object>) => void;
};
declare const CodeEditor: (props: CodeEditorParams) => React.JSX.Element;
export default CodeEditor;

import React from 'react';
import './ForoEditor.css';
import 'react-quill/dist/quill.snow.css';
type FilesUploaderProps = {
    name: string;
    value: string;
    style: object;
    getInformation: (key: string, value: string) => void;
};
declare const ForoEditor: (props: FilesUploaderProps) => React.JSX.Element;
export default ForoEditor;

import React from 'react';
type CodeEditorParams = {
    deselectedColor: string;
    selectedColor: string;
};
declare const Favorites: (props: CodeEditorParams) => React.JSX.Element;
export default Favorites;

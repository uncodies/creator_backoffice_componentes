import React from 'react';
import './FilesUploader.css';
type FilesUploaderProps = {
    name: string;
    endpoint: string;
    fileType: string;
    hideButton: string;
    hidePreview: string;
    maxFiles: string;
    onUpload: (key: string, value: any) => void;
    bucketName?: any;
    idProject?: any;
};
declare const FilesUploader: (props: FilesUploaderProps) => React.JSX.Element;
export default FilesUploader;

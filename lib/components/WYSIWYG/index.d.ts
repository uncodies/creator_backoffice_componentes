import React from 'react';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./WYSIWYG.css";
type WYSIWYGParams = {
    name: string;
    value: string;
    onChange: Function;
    style: object;
    getInformation: (key: string, value: string) => void;
};
declare const WYSIWYG: ({ name, value, style, getInformation }: WYSIWYGParams) => React.JSX.Element;
export default WYSIWYG;

const nrwlConfig = require("@nrwl/react/plugins/webpack.js");
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
module.exports = (config, context) => {
  // first call it so that @nrwl/react plugin adds its configs
  nrwlConfig(config);

  return {
    ...config,
    node: undefined,
    plugins: [
      new MonacoWebpackPlugin({
        // available options are documented at https://github.com/microsoft/monaco-editor/blob/main/webpack-plugin/README.md#options
        languages: ['json']
      })
    ]
  };
};

import React from "react";
import { CodeEditor } from "../components";
import { action } from "@storybook/addon-actions";

export default {
  title: "Components/CodeEditor",
  component: CodeEditor,
  argTypes: {},
};

const Template = (args: any) => (
  <div style={{ width: "1000px", height: "700px" }}>
    <CodeEditor {...args} />
  </div>
);

export const Main = Template.bind({});

Main.args = {
  name: 'prop',
  language: 'javascript',
  theme: 'light',
  value: `()=>{
    fdgdfgdfgdf.dgdfgd();
    dfgdfgd.gfdgdfgdf.dgdfgdfgd();
  }`,
  readOnly: "false",
  getInformation:(()=> (key: string, value: string,  errors: Array<object>) => {
    console.log(key);
    console.log(value);
    console.log(errors);
  })()
};

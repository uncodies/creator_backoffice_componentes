import React from "react";
import { FilesUploader } from "../components";
import { action } from "@storybook/addon-actions";

export default {
  title: "Components/FilesUploader",
  component: FilesUploader,
  argTypes: {},
}

const Template = (args: any) => (
  <div style={{ width: "1000px", height: "500px" }}>
    <FilesUploader {...args} />
  </div>
);

export const Main = Template.bind({});

Main.args = {
  name: 'x_logo_url',
  endpoint: "",
  fileType: "FILE",
  hideButton: "true",
  hidePreview: "true",
  maxFiles: "1",
  updateValue: (data: any) => action(data)(data),
  imageSource: "",
  design: "SIMPLE",
  bucketName: "uncodiepems",
  idProject: 287,
  onUpload: (key: string, value: any) => {
    console.log(key);
    console.log(value);
  }
};

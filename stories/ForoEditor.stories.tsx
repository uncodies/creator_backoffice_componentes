import React from "react";
import { ForoEditor } from "../components";
import { action } from "@storybook/addon-actions";
import { ComponentStory, ComponentMeta } from "@storybook/react";

export default {
  title: "Components/ForoEditor",
  component: ForoEditor,
  argTypes: {},
} as ComponentMeta<typeof ForoEditor>;

const Template: ComponentStory<typeof ForoEditor> = (args: any) => (
  <div style={{ width: "1000px", height: "500px" }}>
    <ForoEditor {...args} />
  </div>
);

export const Main = Template.bind({});

Main.args = {
  name: 'x_logo_url',
  value: "<p>asdasd</p>",
  style: {},
  getInformation: (key: string, value: string) => {
    console.log(key);
    console.log(value);
  }
};

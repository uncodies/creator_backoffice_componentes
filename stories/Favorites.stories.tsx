import React from "react";
import { Favorites } from "../components";
import { action } from "@storybook/addon-actions";
import { ComponentStory, ComponentMeta } from "@storybook/react";

export default {
  title: "Components/Favorites",
  component: Favorites,
  argTypes: {},
} as ComponentMeta<typeof Favorites>;

const Template: ComponentStory<typeof Favorites> = (args: any) => (
  <div style={{ width: "1000px", height: "700px" }}>
    <Favorites {...args} />
  </div>
);

export const Main = Template.bind({});

Main.args = {
  deselectedColor: 'black',
  selectedColor: '#98C9BA'
};

import React from 'react';
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { convertToRaw, ContentState, EditorState } from 'draft-js';
import "./WYSIWYG.css";

type WYSIWYGParams = {
  name: string;
  value: string;
  onChange: Function;
  style: object;
  getInformation: (key: string, value: string) => void;
}

const WYSIWYG = ({ name, value, style, getInformation }: WYSIWYGParams) => {
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  const getInitialState = (defaultValue: any) => {
    if (defaultValue) {
      const { contentBlocks, entityMap } = htmlToDraft(defaultValue);
      const state = ContentState.createFromBlockArray(contentBlocks, entityMap);
      return EditorState.createWithContent(state);
    } else {
      return EditorState.createEmpty()
    }
  }
  React.useEffect(() => {
    if(value) {
      const initialState = getInitialState(value)
      setEditorState(initialState)
    }
  }, [value]);
  const handleEditorState = (editorState: EditorState) => {
    setEditorState(editorState);
    getInformation(name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
  }
  return (
    <Editor
      editorStyle={{...style, backgroundColor: 'white'}}
      editorState={editorState}
      toolbarClassName="toolbarClassName"
      wrapperClassName="wrapperClassName"
      editorClassName="editorClassName"
      onEditorStateChange={handleEditorState}
    />
  )
}
export default WYSIWYG;
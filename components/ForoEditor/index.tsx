import React from 'react';
import ReactQuill from 'react-quill';
import './ForoEditor.css'
import 'react-quill/dist/quill.snow.css';

type FilesUploaderProps = {
  name: string;
  value: string;
  style: object;
  getInformation: (key: string, value: string) => void;
};

const modules = {
  toolbar: [
    [{ 'header': [1, 2, false] }],
    ['bold', 'italic', 'underline','strike', 'blockquote'],
    [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
    ['link', 'image'],
    ['clean']
  ],
}
const formats = [
  'header',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image'
]

const ForoEditor = (props: FilesUploaderProps) => {
  const { name, value, style, getInformation } = props;
  const [content, setContent] = React.useState(value || '');
  React.useEffect(() => {
    getInformation(name, content);
  }, [content])
  return (
    <ReactQuill modules={modules} formats={formats} theme="snow" value={content} onChange={setContent} className='personal-edit-quill' style={style} />
  );
};

export default ForoEditor;

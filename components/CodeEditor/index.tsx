import React from 'react';
import Editor, { DiffEditor, useMonaco, loader } from '@monaco-editor/react';
import Modal from 'react-modal';
import './CodeEditor.css';
import { AiOutlineExpand } from "react-icons/ai";
const customStyles = {
  overlay: {
    backgroundColor: 'rgb(255 255 255 / 63%)',
    backdropFilter: 'blur(16px)',
    zIndex: 4
  }
}
type ThemeType = 'light' | 'vs-dark';

type CodeEditorParams = {
  name: string
  language: string
  theme: ThemeType
  readOnly?: boolean | string
  value: string
  getInformation: (key: string, value: string, errors: Array<object>) => void;
}

const CodeEditor = (props: CodeEditorParams) => {
  const { name, language, theme, value, readOnly, getInformation } = props;
  const [content, setEditorValue] = React.useState(value || '');
  const [errors, setErrors] = React.useState([{}]);
  const [modalIsOpen, setIsOpen] = React.useState(false);
  React.useEffect(() => {
    if(value !== content)
      getInformation(name, content, errors);
  }, [content, errors]);
  React.useEffect(() => {
    setEditorValue(value)
  }, [value]);

  console.log('***code editor update work fine***+')
  if(modalIsOpen)
    return(
      <Modal
        isOpen={modalIsOpen}
        closeTimeoutMS={2000}
        onAfterOpen={()=>{}}
        onRequestClose={()=>{ setIsOpen(false) }}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <Editor
          defaultLanguage={language}
          theme={theme}
          value={content}
          onChange={e => (!readOnly || readOnly === 'false') && !!e && setEditorValue(e)}
          width="100%"
          height="100%"
        />
      </Modal>
    )
  return (
    <div className='editor-container'>
      <div className='button-expand' onClick={()=>{ setIsOpen(true) }}><AiOutlineExpand /></div>
      <Editor
        defaultLanguage={language}
        theme={theme}
        value={content}
        onChange={e => (!readOnly || readOnly === 'false' ) && !!e && setEditorValue(e)}
        width="100%"
        height="100%"
      />
    </div>
    
  )
}

export default CodeEditor;


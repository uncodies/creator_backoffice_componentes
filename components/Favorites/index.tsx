import React from 'react';
import IconButton from '@mui/material/IconButton';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import FavoriteIcon from '@mui/icons-material/Favorite';

type CodeEditorParams = {
  deselectedColor: string;
  selectedColor: string
}

const Favorites = (props: CodeEditorParams) => {
  const { deselectedColor, selectedColor } = props;
  const [selected, setSelected] = React.useState(false);
  return (
    <IconButton onClick={() => setSelected(!selected)}>
      { selected ? <FavoriteIcon style={{ color: selectedColor }}/> : <FavoriteBorderIcon style={{ color: deselectedColor }}/> }
    </IconButton>
  )
}

export default Favorites;


import React from 'react';
import Dropzone, {DropzoneRef} from 'react-dropzone';
import { uploadFiles } from '../../scripts/scripts';
import './FilesUploader.css'

const dropzoneRef = React.createRef<DropzoneRef>();
const openDialog = () => {
  // Note that the ref is set async,
  // so it might be null at some point 
  if (dropzoneRef.current) {
    dropzoneRef.current.open()
  }
};

type FilesUploaderProps = {
  name: string;
  endpoint: string;
  fileType: string;
  hideButton: string;
  hidePreview: string;
  maxFiles: string;
  onUpload: (key: string, value: any) => void;
  bucketName?: any;
  idProject?: any;
};

const FilesUploader = (props: FilesUploaderProps) => {
  const { name, endpoint, fileType, hideButton, hidePreview, maxFiles, onUpload, bucketName, idProject } = props;
  const [fileTypeText, setFileTypeText] = React.useState<string>('PHOTO')
  const handleUpload = async (acceptedFiles: any) => {
    const promises = await uploadFiles(acceptedFiles, endpoint, bucketName, idProject)
    Promise.all(promises)
      .then((res) => {
        onUpload(name, res);
      })
      .catch((err) => console.error(err));
  }
  React.useEffect(() => {
    if(fileType === 'FILE') setFileTypeText('FILE')
  }, []);
  return (
    <Dropzone ref={dropzoneRef as React.RefObject<DropzoneRef>} noClick noKeyboard onDrop={handleUpload} maxFiles={maxFiles ? parseInt(maxFiles) : 4}>
      {({getRootProps, getInputProps, acceptedFiles}) => {
        return (
          <div className="container" style={{ cursor: hideButton === 'true' ? 'pointer': 'initial' }} onClick={hideButton === 'true' ? openDialog:  () => {}}>
            <div {...getRootProps({className: 'dropzone'})}>
              <input {...getInputProps()} />
              <span className='span-theme'>Drag the {fileTypeText.toLocaleLowerCase()} here</span>
              {hideButton !== "true" && 
                <React.Fragment>
                  <span className='dropzone-text-12 span-theme'>or if you prefer...</span>
                  <button
                    type="button"
                    onClick={openDialog}
                    className='button-support'
                  >
                    SELECT {fileTypeText} TO UPLOAD
                  </button>
                </React.Fragment>
              }
            </div>
            <aside>
              <h4 className='h4-theme'>Files</h4>
              <ul>
                {acceptedFiles.map((file: any) => (
                  <li key={file.path}>
                    {file.path} - {file.size} bytes
                  </li>
                ))}
              </ul>
            </aside>
            {hidePreview !== "true" &&
              <div className='div-all-images-container'>
                {acceptedFiles.map((file: any) => { if(!file.type.includes('image')) return null; return (
                  <div className='image-container' key={file.name}>
                    <div className='thumb-inner'>
                      <img
                        className='image-preview'
                        src={ URL.createObjectURL(file)}
                        onLoad={() => { URL.revokeObjectURL( URL.createObjectURL(file)) }}
                      />
                    </div>
                  </div>
                )})}
              </div>
            }
          </div>
        );
      }}
    </Dropzone>
  );
};

export default FilesUploader;
